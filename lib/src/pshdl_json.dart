//    PSHDL is a library and (trans-)compiler for PSHDL input. It generates
//    output suitable for implementation or simulation of it.
//    
//    Copyright (C) 2014 Karsten Becker (feedback (at) pshdl (dot) org)
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    This License does not grant permission to use the trade names, trademarks,
//    service marks, or product names of the Licensor, except as required for 
//    reasonable and customary use in describing the origin of the Work.
//
//Contributors:
//    Karsten Becker - initial API and implementation

part of pshdl_api;

class Activity {
  final String id;
  final String description;
  final int uniqueID;
  static int _idCounter=0;
  Activity(this.id, this.description) :uniqueID=_idCounter++{
  }
  operator ==(Activity other) {
    return uniqueID==other.uniqueID;
  }
  int get hashCode => uniqueID;
  String toString()=>"$id: $description ($uniqueID)";
}
abstract class IRepoInfo {
  String id;
  Iterable<IFileInfo> files=[];
  num lastValidation;
  String jsonVersion;
  Map toJson();
}
class RepoInfo extends IRepoInfo {
  Map _jsonMap;
  RepoInfo._create(this._jsonMap);

  factory RepoInfo.empty() => new RepoInfo._create({});
  factory RepoInfo.fromJson(Map map) => map==null ? null : new RepoInfo._create(map); 
  factory RepoInfo.fromJsonString(string) => new RepoInfo._create(JSON.decode(string));

  String toString() => JSON.encode(_jsonMap);
  Map toJson() => _jsonMap;

  set id(String newVal) => _jsonMap["id"]=newVal;
  @reflectable
  String get id => _jsonMap["id"];

  set files(Iterable<IFileInfo> newList) => _jsonMap["files"] = newList.map((IFileInfo o)=>o.toJson()).toList();
  @reflectable
  Iterable<IFileInfo> get files {
    List list=_jsonMap["files"];
    if (list==null) return [];
    return list.where((o) => o!=null).map( (o) => new FileInfo.fromJson(o) );
  }


  set lastValidation(num newVal) => _jsonMap["lastValidation"]=newVal;
  @reflectable
  num get lastValidation => _jsonMap["lastValidation"];

  set jsonVersion(String newVal) => _jsonMap["jsonVersion"]=newVal;
  @reflectable
  String get jsonVersion => _jsonMap["jsonVersion"];

}

abstract class IAdviseInfo {
  String message;
  String explanation;
  List<String> solutions=[];
  Map toJson();
}
class AdviseInfo extends IAdviseInfo {
  Map _jsonMap;
  AdviseInfo._create(this._jsonMap);

  factory AdviseInfo.empty() => new AdviseInfo._create({});
  factory AdviseInfo.fromJson(Map map) => map==null ? null : new AdviseInfo._create(map); 
  factory AdviseInfo.fromJsonString(string) => new AdviseInfo._create(JSON.decode(string));

  String toString() => JSON.encode(_jsonMap);
  Map toJson() => _jsonMap;

  set message(String newVal) => _jsonMap["message"]=newVal;
  @reflectable
  String get message => _jsonMap["message"];

  set explanation(String newVal) => _jsonMap["explanation"]=newVal;
  @reflectable
  String get explanation => _jsonMap["explanation"];

  set solutions(List<String> newVal) => _jsonMap["solutions"] = newVal;
  List<String> get solutions => _jsonMap["solutions"];

}

abstract class ICompileInfo {
  num created;
  String creator;
  Iterable<IProblemInfo> problems=[];
  Iterable<IFileRecord> files=[];
  Map toJson();
}
class CompileInfo extends ICompileInfo {
  Map _jsonMap;
  CompileInfo._create(this._jsonMap);

  factory CompileInfo.empty() => new CompileInfo._create({});
  factory CompileInfo.fromJson(Map map) => map==null ? null : new CompileInfo._create(map); 
  factory CompileInfo.fromJsonString(string) => new CompileInfo._create(JSON.decode(string));

  String toString() => JSON.encode(_jsonMap);
  Map toJson() => _jsonMap;

  set created(num newVal) => _jsonMap["created"]=newVal;
  @reflectable
  num get created => _jsonMap["created"];

  set creator(String newVal) => _jsonMap["creator"]=newVal;
  @reflectable
  String get creator => _jsonMap["creator"];

  set problems(Iterable<IProblemInfo> newList) => _jsonMap["problems"] = newList.map((IProblemInfo o)=>o.toJson()).toList();
  @reflectable
  Iterable<IProblemInfo> get problems {
    List list=_jsonMap["problems"];
    if (list==null) return [];
    return list.where((o) => o!=null).map( (o) => new ProblemInfo.fromJson(o) );
  }


  set files(Iterable<IFileRecord> newList) => _jsonMap["files"] = newList.map((IFileRecord o)=>o.toJson()).toList();
  @reflectable
  Iterable<IFileRecord> get files {
    List list=_jsonMap["files"];
    if (list==null) return [];
    return list.where((o) => o!=null).map( (o) => new FileRecord.fromJson(o) );
  }


}

abstract class IFileInfo {
  ICompileInfo info;
  String category;
  IFileRecord record;
  CheckType syntax;
  FileType type;
  Iterable<IModuleInformation> moduleInfos=[];
  Map toJson();
}
class FileInfo extends IFileInfo {
  Map _jsonMap;
  FileInfo._create(this._jsonMap);

  factory FileInfo.empty() => new FileInfo._create({});
  factory FileInfo.fromJson(Map map) => map==null ? null : new FileInfo._create(map); 
  factory FileInfo.fromJsonString(string) => new FileInfo._create(JSON.decode(string));

  String toString() => JSON.encode(_jsonMap);
  Map toJson() => _jsonMap;

  set info(CompileInfo newVal) => _jsonMap["info"] = newVal==null?null:newVal.toJson();
  @reflectable
  CompileInfo get info => new CompileInfo.fromJson(_jsonMap["info"]);

  set category(String newVal) => _jsonMap["category"]=newVal;
  @reflectable
  String get category => _jsonMap["category"];

  set record(FileRecord newVal) => _jsonMap["record"] = newVal==null?null:newVal.toJson();
  @reflectable
  FileRecord get record => new FileRecord.fromJson(_jsonMap["record"]);

  set syntax(CheckType newVal) => _jsonMap["syntax"] = newVal==null?null:newVal.name;
  @reflectable
  CheckType get syntax => CheckType.fromString(_jsonMap["syntax"]);

  set type(FileType newVal) => _jsonMap["type"] = newVal==null?null:newVal.name;
  @reflectable
  FileType get type => FileType.fromString(_jsonMap["type"]);

  set moduleInfos(Iterable<IModuleInformation> newList) => _jsonMap["moduleInfos"] = newList.map((IModuleInformation o)=>o.toJson()).toList();
  @reflectable
  Iterable<IModuleInformation> get moduleInfos {
    List list=_jsonMap["moduleInfos"];
    if (list==null) return [];
    return list.where((o) => o!=null).map( (o) => new ModuleInformation.fromJson(o) );
  }


}

abstract class IFileRecord {
  String fileURI;
  String relPath;
  num lastModified;
  String hash;
  Map toJson();
}
class FileRecord extends IFileRecord {
  Map _jsonMap;
  FileRecord._create(this._jsonMap);

  factory FileRecord.empty() => new FileRecord._create({});
  factory FileRecord.fromJson(Map map) => map==null ? null : new FileRecord._create(map); 
  factory FileRecord.fromJsonString(string) => new FileRecord._create(JSON.decode(string));

  String toString() => JSON.encode(_jsonMap);
  Map toJson() => _jsonMap;

  set fileURI(String newVal) => _jsonMap["fileURI"]=newVal;
  @reflectable
  String get fileURI => _jsonMap["fileURI"];

  set relPath(String newVal) => _jsonMap["relPath"]=newVal;
  @reflectable
  String get relPath => _jsonMap["relPath"];

  set lastModified(num newVal) => _jsonMap["lastModified"]=newVal;
  @reflectable
  num get lastModified => _jsonMap["lastModified"];

  set hash(String newVal) => _jsonMap["hash"]=newVal;
  @reflectable
  String get hash => _jsonMap["hash"];

}

abstract class ILocationInfo {
  num length;
  num line;
  num offsetInLine;
  num totalOffset;
  String path;
  Map toJson();
}
class LocationInfo extends ILocationInfo {
  Map _jsonMap;
  LocationInfo._create(this._jsonMap);

  factory LocationInfo.empty() => new LocationInfo._create({});
  factory LocationInfo.fromJson(Map map) => map==null ? null : new LocationInfo._create(map); 
  factory LocationInfo.fromJsonString(string) => new LocationInfo._create(JSON.decode(string));

  String toString() => JSON.encode(_jsonMap);
  Map toJson() => _jsonMap;

  set length(num newVal) => _jsonMap["length"]=newVal;
  @reflectable
  num get length => _jsonMap["length"];

  set line(num newVal) => _jsonMap["line"]=newVal;
  @reflectable
  num get line => _jsonMap["line"];

  set offsetInLine(num newVal) => _jsonMap["offsetInLine"]=newVal;
  @reflectable
  num get offsetInLine => _jsonMap["offsetInLine"];

  set totalOffset(num newVal) => _jsonMap["totalOffset"]=newVal;
  @reflectable
  num get totalOffset => _jsonMap["totalOffset"];

  set path(String newVal) => _jsonMap["path"]=newVal;
  @reflectable
  String get path => _jsonMap["path"];

}

abstract class IMessage {
  static const String VENDOR='P';
  static const String WORKSPACE='P:WORKSPACE';
  static const String WORK_DELETED='P:WORKSPACE:DELETED';
  static const String WORK_ADDED='P:WORKSPACE:ADDED';
  static const String WORK_UPDATED='P:WORKSPACE:UPDATED';
  static const String WORK_CREATED_WORKSPACE='P:WORKSPACE:CREATED_WORKSPACE';
  static const String COMPILER='P:COMPILER';
  static const String COMP_VHDL='P:COMPILER:VHDL';
  static const String COMP_PSEX='P:COMPILER:PSEX';
  static const String COMP_C_CODE='P:COMPILER:C';
  static const String COMP_DART_CODE='P:COMPILER:DART';
  static const String COMP_JAVA_CODE='P:COMPILER:JAVA';
  static const String COMP_JAVA_SCRIPT_CODE='P:COMPILER:JAVASCRIPT';
  static const String COMP_SYNTHESIS='P:COMPILER:SYNTHESIS';
  static const String SERVICE='P:SERVICE';
  static const String SERVICE_DISCOVER='P:SERVICE:DISCOVER';
  static const String SYNTHESIS='P:SYNTHESIS';
  static const String SYNTHESIS_RUN='P:SYNTHESIS:RUN';
  static const String SYNTHESIS_PROGRESS='P:SYNTHESIS:PROGRESS';
  static const String SYNTHESIS_AVAILABLE='P:SYNTHESIS:AVAILABLE';
  static const String BOARD='P:BOARD';
  static const String BOARD_AVAILABLE='P:BOARD:AVAILABLE';
  static const String BOARD_CONFIGURE='P:BOARD:CONFIGURE';
  static const String BOARD_PROGRESS='P:BOARD:PROGRESS';
  static const String CLIENT='P:CLIENT';
  static const String CLIENT_CONNECTED='P:CLIENT:CONNECTED';
  static const String CLIENT_DISCONNECTED='P:CLIENT:DISCONNECTED';
  String subject;
  String clientID;
  String msgType;
  num timeStamp;
  Object contents;
  Map toJson();
}
class Message extends IMessage {
  Map _jsonMap;
  Message._create(this._jsonMap);

  factory Message.empty() => new Message._create({});
  factory Message.fromJson(Map map) => map==null ? null : new Message._create(map); 
  factory Message.fromJsonString(string) => new Message._create(JSON.decode(string));

  String toString() => JSON.encode(_jsonMap);
  Map toJson() => _jsonMap;

  set subject(String newVal) => _jsonMap["subject"]=newVal;
  @reflectable
  String get subject => _jsonMap["subject"];

  set clientID(String newVal) => _jsonMap["clientID"]=newVal;
  @reflectable
  String get clientID => _jsonMap["clientID"];

  set msgType(String newVal) => _jsonMap["msgType"]=newVal;
  @reflectable
  String get msgType => _jsonMap["msgType"];

  set timeStamp(num newVal) => _jsonMap["timeStamp"]=newVal;
  @reflectable
  num get timeStamp => _jsonMap["timeStamp"];

  set contents(Object newVal) => _jsonMap["contents"]=newVal;
  @reflectable
  Object get contents => _jsonMap["contents"];

}

abstract class IProblemInfo {
  IAdviseInfo advise;
  ILocationInfo location;
  String errorCode;
  String info;
  num pid;
  ProblemSeverity severity;
  Map toJson();
}
class ProblemInfo extends IProblemInfo {
  Map _jsonMap;
  ProblemInfo._create(this._jsonMap);

  factory ProblemInfo.empty() => new ProblemInfo._create({});
  factory ProblemInfo.fromJson(Map map) => map==null ? null : new ProblemInfo._create(map); 
  factory ProblemInfo.fromJsonString(string) => new ProblemInfo._create(JSON.decode(string));

  String toString() => JSON.encode(_jsonMap);
  Map toJson() => _jsonMap;

  set advise(AdviseInfo newVal) => _jsonMap["advise"] = newVal==null?null:newVal.toJson();
  @reflectable
  AdviseInfo get advise => new AdviseInfo.fromJson(_jsonMap["advise"]);

  set location(LocationInfo newVal) => _jsonMap["location"] = newVal==null?null:newVal.toJson();
  @reflectable
  LocationInfo get location => new LocationInfo.fromJson(_jsonMap["location"]);

  set errorCode(String newVal) => _jsonMap["errorCode"]=newVal;
  @reflectable
  String get errorCode => _jsonMap["errorCode"];

  set info(String newVal) => _jsonMap["info"]=newVal;
  @reflectable
  String get info => _jsonMap["info"];

  set pid(num newVal) => _jsonMap["pid"]=newVal;
  @reflectable
  num get pid => _jsonMap["pid"];

  set severity(ProblemSeverity newVal) => _jsonMap["severity"] = newVal==null?null:newVal.name;
  @reflectable
  ProblemSeverity get severity => ProblemSeverity.fromString(_jsonMap["severity"]);

}

class FileType {
  static const FileType pshdl=const FileType._create(0,"pshdl");
  static const FileType vhdl=const FileType._create(1,"vhdl");
  static const FileType verilog=const FileType._create(2,"verilog");
  static const FileType markdown=const FileType._create(3,"markdown");
  static const FileType cpp=const FileType._create(4,"cpp");
  static const FileType html=const FileType._create(5,"html");
  static const FileType cHeader=const FileType._create(6,"cHeader");
  static const FileType javascript=const FileType._create(7,"javascript");
  static const FileType json=const FileType._create(8,"json");
  static const FileType dart=const FileType._create(9,"dart");
  static const FileType unknown=const FileType._create(10,"unknown");
  static final List<FileType> _values=[pshdl, vhdl, verilog, markdown, cpp, html, cHeader, javascript, json, dart, unknown];
  final int _id;
  final String _name;
  @reflectable
  int get ordinal=>_id;
  @reflectable
  String get name=>_name;
  
  const FileType._create(this._id, this._name);
  
  String toString()=>_name;
  
  static FileType fromString(String name){
    if (name==null) return null;
    return _values.firstWhere((FileType ct)=>ct.name==name);
  }
}
class CheckType {
  static const CheckType unknown=const CheckType._create(0,"unknown");
  static const CheckType syntax_ok=const CheckType._create(1,"syntax_ok");
  static const CheckType ok=const CheckType._create(2,"ok");
  static const CheckType warnings=const CheckType._create(3,"warnings");
  static const CheckType errors=const CheckType._create(4,"errors");
  static const CheckType syntax_error=const CheckType._create(5,"syntax_error");
  static final List<CheckType> _values=[unknown, syntax_ok, ok, warnings, errors, syntax_error];
  final int _id;
  final String _name;
  @reflectable
  int get ordinal=>_id;
  @reflectable
  String get name=>_name;
  
  const CheckType._create(this._id, this._name);
  
  String toString()=>_name;
  
  static CheckType fromString(String name){
    if (name==null) return null;
    return _values.firstWhere((CheckType ct)=>ct.name==name);
  }
}
class ProblemSeverity {
  static const ProblemSeverity INFO=const ProblemSeverity._create(0,"INFO");
  static const ProblemSeverity WARNING=const ProblemSeverity._create(1,"WARNING");
  static const ProblemSeverity ERROR=const ProblemSeverity._create(2,"ERROR");
  static final List<ProblemSeverity> _values=[INFO, WARNING, ERROR];
  final int _id;
  final String _name;
  @reflectable
  int get ordinal=>_id;
  @reflectable
  String get name=>_name;
  
  const ProblemSeverity._create(this._id, this._name);
  
  String toString()=>_name;
  
  static ProblemSeverity fromString(String name){
    if (name==null) return null;
    return _values.firstWhere((ProblemSeverity ct)=>ct.name==name);
  }
}
class CodeTarget {
  static const CodeTarget VHDL=const CodeTarget._create(0,"VHDL");
  static const CodeTarget JAVA=const CodeTarget._create(1,"JAVA");
  static const CodeTarget C=const CodeTarget._create(2,"C");
  static const CodeTarget DART=const CodeTarget._create(3,"DART");
  static const CodeTarget JAVA_SCRIPT=const CodeTarget._create(4,"JAVA_SCRIPT");
  static const CodeTarget PSEX=const CodeTarget._create(5,"PSEX");
  static final List<CodeTarget> _values=[VHDL, JAVA, C, DART, JAVA_SCRIPT, PSEX];
  final int _id;
  final String _name;
  @reflectable
  int get ordinal=>_id;
  @reflectable
  String get name=>_name;
  
  const CodeTarget._create(this._id, this._name);
  
  String toString()=>_name;
  
  static CodeTarget fromString(String name){
    if (name==null) return null;
    return _values.firstWhere((CodeTarget ct)=>ct.name==name);
  }
}
abstract class IBoardSpecSettings {
  String boardName;
  String description;
  IFPGASpec fpga;
  Iterable<IPinSpecGroup> pinGroups=[];
  IInteractiveMap map;
  static const String BOARD_CATEGORY='Board';
  static const String VERSION='0.1';
  String category;
  String version;
  Map toJson();
}
class BoardSpecSettings extends IBoardSpecSettings {
  Map _jsonMap;
  BoardSpecSettings._create(this._jsonMap);

  factory BoardSpecSettings.empty() => new BoardSpecSettings._create({});
  factory BoardSpecSettings.fromJson(Map map) => map==null ? null : new BoardSpecSettings._create(map); 
  factory BoardSpecSettings.fromJsonString(string) => new BoardSpecSettings._create(JSON.decode(string));

  String toString() => JSON.encode(_jsonMap);
  Map toJson() => _jsonMap;

  set boardName(String newVal) => _jsonMap["boardName"]=newVal;
  @reflectable
  String get boardName => _jsonMap["boardName"];

  set description(String newVal) => _jsonMap["description"]=newVal;
  @reflectable
  String get description => _jsonMap["description"];

  set fpga(FPGASpec newVal) => _jsonMap["fpga"] = newVal==null?null:newVal.toJson();
  @reflectable
  FPGASpec get fpga => new FPGASpec.fromJson(_jsonMap["fpga"]);

  set pinGroups(Iterable<IPinSpecGroup> newList) => _jsonMap["pinGroups"] = newList.map((IPinSpecGroup o)=>o.toJson()).toList();
  @reflectable
  Iterable<IPinSpecGroup> get pinGroups {
    List list=_jsonMap["pinGroups"];
    if (list==null) return [];
    return list.where((o) => o!=null).map( (o) => new PinSpecGroup.fromJson(o) );
  }


  set map(InteractiveMap newVal) => _jsonMap["map"] = newVal==null?null:newVal.toJson();
  @reflectable
  InteractiveMap get map => new InteractiveMap.fromJson(_jsonMap["map"]);

  set category(String newVal) => _jsonMap["category"]=newVal;
  @reflectable
  String get category => _jsonMap["category"];

  set version(String newVal) => _jsonMap["version"]=newVal;
  @reflectable
  String get version => _jsonMap["version"];

}

abstract class IInteractiveMap {
  String svg;
  Map idMaps;
  Map toJson();
}
class InteractiveMap extends IInteractiveMap {
  Map _jsonMap;
  InteractiveMap._create(this._jsonMap);

  factory InteractiveMap.empty() => new InteractiveMap._create({});
  factory InteractiveMap.fromJson(Map map) => map==null ? null : new InteractiveMap._create(map); 
  factory InteractiveMap.fromJsonString(string) => new InteractiveMap._create(JSON.decode(string));

  String toString() => JSON.encode(_jsonMap);
  Map toJson() => _jsonMap;

  set svg(String newVal) => _jsonMap["svg"]=newVal;
  @reflectable
  String get svg => _jsonMap["svg"];

  set idMaps(Map newVal) => _jsonMap["idMaps"]=newVal;
  @reflectable
  Map get idMaps => _jsonMap["idMaps"];

}

abstract class IFPGASpec {
  String vendor;
  String family;
  String partNumber;
  Map toJson();
}
class FPGASpec extends IFPGASpec {
  Map _jsonMap;
  FPGASpec._create(this._jsonMap);

  factory FPGASpec.empty() => new FPGASpec._create({});
  factory FPGASpec.fromJson(Map map) => map==null ? null : new FPGASpec._create(map); 
  factory FPGASpec.fromJsonString(string) => new FPGASpec._create(JSON.decode(string));

  String toString() => JSON.encode(_jsonMap);
  Map toJson() => _jsonMap;

  set vendor(String newVal) => _jsonMap["vendor"]=newVal;
  @reflectable
  String get vendor => _jsonMap["vendor"];

  set family(String newVal) => _jsonMap["family"]=newVal;
  @reflectable
  String get family => _jsonMap["family"];

  set partNumber(String newVal) => _jsonMap["partNumber"]=newVal;
  @reflectable
  String get partNumber => _jsonMap["partNumber"];

}

abstract class IPinSpecGroup {
  String name;
  String description;
  Iterable<IPinSpec> pins=[];
  Map toJson();
}
class PinSpecGroup extends IPinSpecGroup {
  Map _jsonMap;
  PinSpecGroup._create(this._jsonMap);

  factory PinSpecGroup.empty() => new PinSpecGroup._create({});
  factory PinSpecGroup.fromJson(Map map) => map==null ? null : new PinSpecGroup._create(map); 
  factory PinSpecGroup.fromJsonString(string) => new PinSpecGroup._create(JSON.decode(string));

  String toString() => JSON.encode(_jsonMap);
  Map toJson() => _jsonMap;

  set name(String newVal) => _jsonMap["name"]=newVal;
  @reflectable
  String get name => _jsonMap["name"];

  set description(String newVal) => _jsonMap["description"]=newVal;
  @reflectable
  String get description => _jsonMap["description"];

  set pins(Iterable<IPinSpec> newList) => _jsonMap["pins"] = newList.map((IPinSpec o)=>o.toJson()).toList();
  @reflectable
  Iterable<IPinSpec> get pins {
    List list=_jsonMap["pins"];
    if (list==null) return [];
    return list.where((o) => o!=null).map( (o) => new PinSpec.fromJson(o) );
  }


}

abstract class IPinSpec {
  static const String IOSTANDARD='IOSTANDARD';
  static const String NO_VALUE='#NO_VALUE';
  static const String PULL_UP='UP';
  static const String PULL_DOWN='DOWN';
  static const String PULL='PULL';
  static const String INVERT='INVERT';
  static const String SIG_NONE='#NONE';
  static const String SIG_OPEN='#OPEN';
  static const String SIG_ALLONE='#ALLONE';
  static const String SIG_ALLZERO='#ALLZERO';
  String description;
  String portName;
  String pinLocation;
  Map attributes;
  ITimeSpec timeSpec;
  String assignedSignal;
  Polarity polarity;
  HDLDirection direction;
  Map toJson();
}
class PinSpec extends IPinSpec {
  Map _jsonMap;
  PinSpec._create(this._jsonMap);

  factory PinSpec.empty() => new PinSpec._create({});
  factory PinSpec.fromJson(Map map) => map==null ? null : new PinSpec._create(map); 
  factory PinSpec.fromJsonString(string) => new PinSpec._create(JSON.decode(string));

  String toString() => JSON.encode(_jsonMap);
  Map toJson() => _jsonMap;

  set description(String newVal) => _jsonMap["description"]=newVal;
  @reflectable
  String get description => _jsonMap["description"];

  set portName(String newVal) => _jsonMap["portName"]=newVal;
  @reflectable
  String get portName => _jsonMap["portName"];

  set pinLocation(String newVal) => _jsonMap["pinLocation"]=newVal;
  @reflectable
  String get pinLocation => _jsonMap["pinLocation"];

  set attributes(Map newVal) => _jsonMap["attributes"]=newVal;
  @reflectable
  Map get attributes => _jsonMap["attributes"];

  set timeSpec(TimeSpec newVal) => _jsonMap["timeSpec"] = newVal==null?null:newVal.toJson();
  @reflectable
  TimeSpec get timeSpec => new TimeSpec.fromJson(_jsonMap["timeSpec"]);

  set assignedSignal(String newVal) => _jsonMap["assignedSignal"]=newVal;
  @reflectable
  String get assignedSignal => _jsonMap["assignedSignal"];

  set polarity(Polarity newVal) => _jsonMap["polarity"] = newVal==null?null:newVal.name;
  @reflectable
  Polarity get polarity => Polarity.fromString(_jsonMap["polarity"]);

  set direction(HDLDirection newVal) => _jsonMap["direction"] = newVal==null?null:newVal.name;
  @reflectable
  HDLDirection get direction => HDLDirection.fromString(_jsonMap["direction"]);

}

abstract class ITimeSpec {
  String time;
  String unit;
  Map toJson();
}
class TimeSpec extends ITimeSpec {
  Map _jsonMap;
  TimeSpec._create(this._jsonMap);

  factory TimeSpec.empty() => new TimeSpec._create({});
  factory TimeSpec.fromJson(Map map) => map==null ? null : new TimeSpec._create(map); 
  factory TimeSpec.fromJsonString(string) => new TimeSpec._create(JSON.decode(string));

  String toString() => JSON.encode(_jsonMap);
  Map toJson() => _jsonMap;

  set time(String newVal) => _jsonMap["time"]=newVal;
  @reflectable
  String get time => _jsonMap["time"];

  set unit(String newVal) => _jsonMap["unit"]=newVal;
  @reflectable
  String get unit => _jsonMap["unit"];

}

class Polarity {
  static const Polarity active_low=const Polarity._create(0,"active_low");
  static const Polarity active_high=const Polarity._create(1,"active_high");
  static final List<Polarity> _values=[active_low, active_high];
  final int _id;
  final String _name;
  @reflectable
  int get ordinal=>_id;
  @reflectable
  String get name=>_name;
  
  const Polarity._create(this._id, this._name);
  
  String toString()=>_name;
  
  static Polarity fromString(String name){
    if (name==null) return null;
    return _values.firstWhere((Polarity ct)=>ct.name==name);
  }
}
abstract class ISynthesisSettings {
  static const String VERSION='0.1';
  static const String SYNTHESIS='Synthesis';
  String board;
  String topModule;
  Iterable<IPinSpec> overrides=[];
  Map overrideParameters;
  String category;
  String version;
  Map toJson();
}
class SynthesisSettings extends ISynthesisSettings {
  Map _jsonMap;
  SynthesisSettings._create(this._jsonMap);

  factory SynthesisSettings.empty() => new SynthesisSettings._create({});
  factory SynthesisSettings.fromJson(Map map) => map==null ? null : new SynthesisSettings._create(map); 
  factory SynthesisSettings.fromJsonString(string) => new SynthesisSettings._create(JSON.decode(string));

  String toString() => JSON.encode(_jsonMap);
  Map toJson() => _jsonMap;

  set board(String newVal) => _jsonMap["board"]=newVal;
  @reflectable
  String get board => _jsonMap["board"];

  set topModule(String newVal) => _jsonMap["topModule"]=newVal;
  @reflectable
  String get topModule => _jsonMap["topModule"];

  set overrides(Iterable<IPinSpec> newList) => _jsonMap["overrides"] = newList.map((IPinSpec o)=>o.toJson()).toList();
  @reflectable
  Iterable<IPinSpec> get overrides {
    List list=_jsonMap["overrides"];
    if (list==null) return [];
    return list.where((o) => o!=null).map( (o) => new PinSpec.fromJson(o) );
  }


  set overrideParameters(Map newVal) => _jsonMap["overrideParameters"]=newVal;
  @reflectable
  Map get overrideParameters => _jsonMap["overrideParameters"];

  set category(String newVal) => _jsonMap["category"]=newVal;
  @reflectable
  String get category => _jsonMap["category"];

  set version(String newVal) => _jsonMap["version"]=newVal;
  @reflectable
  String get version => _jsonMap["version"];

}

abstract class IInstanceInfos {
  Iterable<IFileInstances> infos=[];
  Map toJson();
}
class InstanceInfos extends IInstanceInfos {
  Map _jsonMap;
  InstanceInfos._create(this._jsonMap);

  factory InstanceInfos.empty() => new InstanceInfos._create({});
  factory InstanceInfos.fromJson(Map map) => map==null ? null : new InstanceInfos._create(map); 
  factory InstanceInfos.fromJsonString(string) => new InstanceInfos._create(JSON.decode(string));

  String toString() => JSON.encode(_jsonMap);
  Map toJson() => _jsonMap;

  set infos(Iterable<IFileInstances> newList) => _jsonMap["infos"] = newList.map((IFileInstances o)=>o.toJson()).toList();
  @reflectable
  Iterable<IFileInstances> get infos {
    List list=_jsonMap["infos"];
    if (list==null) return [];
    return list.where((o) => o!=null).map( (o) => new FileInstances.fromJson(o) );
  }


}

abstract class IFileInstances {
  String fileName;
  Iterable<IModuleInformation> infos=[];
  Map toJson();
}
class FileInstances extends IFileInstances {
  Map _jsonMap;
  FileInstances._create(this._jsonMap);

  factory FileInstances.empty() => new FileInstances._create({});
  factory FileInstances.fromJson(Map map) => map==null ? null : new FileInstances._create(map); 
  factory FileInstances.fromJsonString(string) => new FileInstances._create(JSON.decode(string));

  String toString() => JSON.encode(_jsonMap);
  Map toJson() => _jsonMap;

  set fileName(String newVal) => _jsonMap["fileName"]=newVal;
  @reflectable
  String get fileName => _jsonMap["fileName"];

  set infos(Iterable<IModuleInformation> newList) => _jsonMap["infos"] = newList.map((IModuleInformation o)=>o.toJson()).toList();
  @reflectable
  Iterable<IModuleInformation> get infos {
    List list=_jsonMap["infos"];
    if (list==null) return [];
    return list.where((o) => o!=null).map( (o) => new ModuleInformation.fromJson(o) );
  }


}

abstract class IModuleInformation {
  String name;
  Iterable<IPort> ports=[];
  List<String> instances=[];
  UnitType type;
  Map toJson();
}
class ModuleInformation extends IModuleInformation {
  Map _jsonMap;
  ModuleInformation._create(this._jsonMap);

  factory ModuleInformation.empty() => new ModuleInformation._create({});
  factory ModuleInformation.fromJson(Map map) => map==null ? null : new ModuleInformation._create(map); 
  factory ModuleInformation.fromJsonString(string) => new ModuleInformation._create(JSON.decode(string));

  String toString() => JSON.encode(_jsonMap);
  Map toJson() => _jsonMap;

  set name(String newVal) => _jsonMap["name"]=newVal;
  @reflectable
  String get name => _jsonMap["name"];

  set ports(Iterable<IPort> newList) => _jsonMap["ports"] = newList.map((IPort o)=>o.toJson()).toList();
  @reflectable
  Iterable<IPort> get ports {
    List list=_jsonMap["ports"];
    if (list==null) return [];
    return list.where((o) => o!=null).map( (o) => new Port.fromJson(o) );
  }


  set instances(List<String> newVal) => _jsonMap["instances"] = newVal;
  List<String> get instances => _jsonMap["instances"];

  set type(UnitType newVal) => _jsonMap["type"] = newVal==null?null:newVal.name;
  @reflectable
  UnitType get type => UnitType.fromString(_jsonMap["type"]);

}

abstract class IPort {
  String name;
  HDLDirection dir;
  HDLPrimitiveType primitive;
  String enumRef;
  String defaultValue;
  num width;
  List<int> dimensions=[];
  List<String> annotations=[];
  Map toJson();
}
class Port extends IPort {
  Map _jsonMap;
  Port._create(this._jsonMap);

  factory Port.empty() => new Port._create({});
  factory Port.fromJson(Map map) => map==null ? null : new Port._create(map); 
  factory Port.fromJsonString(string) => new Port._create(JSON.decode(string));

  String toString() => JSON.encode(_jsonMap);
  Map toJson() => _jsonMap;

  set name(String newVal) => _jsonMap["name"]=newVal;
  @reflectable
  String get name => _jsonMap["name"];

  set dir(HDLDirection newVal) => _jsonMap["dir"] = newVal==null?null:newVal.name;
  @reflectable
  HDLDirection get dir => HDLDirection.fromString(_jsonMap["dir"]);

  set primitive(HDLPrimitiveType newVal) => _jsonMap["primitive"] = newVal==null?null:newVal.name;
  @reflectable
  HDLPrimitiveType get primitive => HDLPrimitiveType.fromString(_jsonMap["primitive"]);

  set enumRef(String newVal) => _jsonMap["enumRef"]=newVal;
  @reflectable
  String get enumRef => _jsonMap["enumRef"];

  set defaultValue(String newVal) => _jsonMap["defaultValue"]=newVal;
  @reflectable
  String get defaultValue => _jsonMap["defaultValue"];

  set width(num newVal) => _jsonMap["width"]=newVal;
  @reflectable
  num get width => _jsonMap["width"];

  set dimensions(List<int> newVal) => _jsonMap["dimensions"] = newVal;
  List<int> get dimensions => _jsonMap["dimensions"];

  set annotations(List<String> newVal) => _jsonMap["annotations"] = newVal;
  List<String> get annotations => _jsonMap["annotations"];

}

class UnitType {
  static const UnitType ENUM=const UnitType._create(0,"ENUM");
  static const UnitType MODULE=const UnitType._create(1,"MODULE");
  static const UnitType INTERFACE=const UnitType._create(2,"INTERFACE");
  static const UnitType TESTBENCH=const UnitType._create(3,"TESTBENCH");
  static final List<UnitType> _values=[ENUM, MODULE, INTERFACE, TESTBENCH];
  final int _id;
  final String _name;
  @reflectable
  int get ordinal=>_id;
  @reflectable
  String get name=>_name;
  
  const UnitType._create(this._id, this._name);
  
  String toString()=>_name;
  
  static UnitType fromString(String name){
    if (name==null) return null;
    return _values.firstWhere((UnitType ct)=>ct.name==name);
  }
}
class HDLDirection {
  static const HDLDirection IN=const HDLDirection._create(0,"IN");
  static const HDLDirection OUT=const HDLDirection._create(1,"OUT");
  static const HDLDirection INOUT=const HDLDirection._create(2,"INOUT");
  static const HDLDirection PARAMETER=const HDLDirection._create(3,"PARAMETER");
  static const HDLDirection CONSTANT=const HDLDirection._create(4,"CONSTANT");
  static const HDLDirection INTERNAL=const HDLDirection._create(5,"INTERNAL");
  static const HDLDirection HIDDEN=const HDLDirection._create(6,"HIDDEN");
  static final List<HDLDirection> _values=[IN, OUT, INOUT, PARAMETER, CONSTANT, INTERNAL, HIDDEN];
  final int _id;
  final String _name;
  @reflectable
  int get ordinal=>_id;
  @reflectable
  String get name=>_name;
  
  const HDLDirection._create(this._id, this._name);
  
  String toString()=>_name;
  
  static HDLDirection fromString(String name){
    if (name==null) return null;
    return _values.firstWhere((HDLDirection ct)=>ct.name==name);
  }
}
class HDLPrimitiveType {
  static const HDLPrimitiveType INT=const HDLPrimitiveType._create(0,"INT");
  static const HDLPrimitiveType UINT=const HDLPrimitiveType._create(1,"UINT");
  static const HDLPrimitiveType INTEGER=const HDLPrimitiveType._create(2,"INTEGER");
  static const HDLPrimitiveType NATURAL=const HDLPrimitiveType._create(3,"NATURAL");
  static const HDLPrimitiveType BIT=const HDLPrimitiveType._create(4,"BIT");
  static const HDLPrimitiveType BITVECTOR=const HDLPrimitiveType._create(5,"BITVECTOR");
  static const HDLPrimitiveType BOOL=const HDLPrimitiveType._create(6,"BOOL");
  static const HDLPrimitiveType STRING=const HDLPrimitiveType._create(7,"STRING");
  static final List<HDLPrimitiveType> _values=[INT, UINT, INTEGER, NATURAL, BIT, BITVECTOR, BOOL, STRING];
  final int _id;
  final String _name;
  @reflectable
  int get ordinal=>_id;
  @reflectable
  String get name=>_name;
  
  const HDLPrimitiveType._create(this._id, this._name);
  
  String toString()=>_name;
  
  static HDLPrimitiveType fromString(String name){
    if (name==null) return null;
    return _values.firstWhere((HDLPrimitiveType ct)=>ct.name==name);
  }
}
abstract class IProgressFeedback {
  ProgressType type;
  num progress;
  num timeStamp;
  String message;
  Map toJson();
}
class ProgressFeedback extends IProgressFeedback {
  Map _jsonMap;
  ProgressFeedback._create(this._jsonMap);

  factory ProgressFeedback.empty() => new ProgressFeedback._create({});
  factory ProgressFeedback.fromJson(Map map) => map==null ? null : new ProgressFeedback._create(map); 
  factory ProgressFeedback.fromJsonString(string) => new ProgressFeedback._create(JSON.decode(string));

  String toString() => JSON.encode(_jsonMap);
  Map toJson() => _jsonMap;

  set type(ProgressType newVal) => _jsonMap["type"] = newVal==null?null:newVal.name;
  @reflectable
  ProgressType get type => ProgressType.fromString(_jsonMap["type"]);

  set progress(num newVal) => _jsonMap["progress"]=newVal;
  @reflectable
  num get progress => _jsonMap["progress"];

  set timeStamp(num newVal) => _jsonMap["timeStamp"]=newVal;
  @reflectable
  num get timeStamp => _jsonMap["timeStamp"];

  set message(String newVal) => _jsonMap["message"]=newVal;
  @reflectable
  String get message => _jsonMap["message"];

}

class ProgressType {
  static const ProgressType report=const ProgressType._create(0,"report");
  static const ProgressType progress=const ProgressType._create(1,"progress");
  static const ProgressType output=const ProgressType._create(2,"output");
  static const ProgressType done=const ProgressType._create(3,"done");
  static const ProgressType information=const ProgressType._create(4,"information");
  static const ProgressType warning=const ProgressType._create(5,"warning");
  static const ProgressType error=const ProgressType._create(6,"error");
  static final List<ProgressType> _values=[report, progress, output, done, information, warning, error];
  final int _id;
  final String _name;
  @reflectable
  int get ordinal=>_id;
  @reflectable
  String get name=>_name;
  
  const ProgressType._create(this._id, this._name);
  
  String toString()=>_name;
  
  static ProgressType fromString(String name){
    if (name==null) return null;
    return _values.firstWhere((ProgressType ct)=>ct.name==name);
  }
}

