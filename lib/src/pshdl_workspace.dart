//    PSHDL is a library and (trans-)compiler for PSHDL input. It generates
//    output suitable for implementation or simulation of it.
//
//    Copyright (C) 2014 Karsten Becker (feedback (at) pshdl (dot) org)
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    This License does not grant permission to use the trade names, trademarks,
//    service marks, or product names of the Licensor, except as required for
//    reasonable and customary use in describing the origin of the Work.
//
//Contributors:
//    Karsten Becker - initial API and implementation

part of pshdl_api;

Workspace currentWorkspace;

//Workspace related messages
final EventType<IFileInfo> workspaceFileDeleted = new EventType<IFileInfo>("workspaceFileDeleted");
final EventType<List<IFileInfo>> workspaceFileAdded = new EventType<List<IFileInfo>>("workspaceFileAdded");
final EventType<List<IFileInfo>> workspaceFileUpdated = new EventType<List<IFileInfo>>("workspaceFileUpdated");
final EventType<IRepoInfo> workspaceRepoUpdate = new EventType<IRepoInfo>("workspaceRepoUpdate");
final EventType<Null> workspaceCreated = new EventType<Null>("workspaceCreated");
final EventType<IRepoInfo> workspaceOpened = new EventType<IRepoInfo>("workspaceOpened");
final EventType<HttpRequest> workspaceError = new EventType<HttpRequest>("workspaceError");
final EventType<IFileInfo> workspaceFileMarkDirty = new EventType<IFileInfo>("workspaceFileMarkDirty");
final EventType<IFileInfo> workspaceFileMarkClean = new EventType<IFileInfo>("workspaceFileMarkClean");

//Activity
final EventType<Activity> activityStarted = new EventType<Activity>("activityStarted");
final EventType<Activity> activityEnded = new EventType<Activity>("activityEnded");

//EventStream messages
final EventType<IMessage> messageReceived = new EventType<IMessage>("messageReceived");


class LocalFile {
  IFileInfo info;
  String relPath;
  String _cachedContent;
  String get cachedContent => _cachedContent;
  set cachedContent(String newValue) {
    _cachedContent = newValue;
    localSHA1correct = false;
  }

  String remoteSHA1;
  String localSHA1;
  bool dirty = false;
  bool localSHA1correct = true;
  EventBus events;
  LocalFile(this.info, String content, this.events) {
    this.relPath = info.record.relPath;
    this.remoteSHA1 = info.record.hash;
    this.cachedContent = content;
    var toEncrypt = new SHA1();
    toEncrypt.add(encodeUtf8(_cachedContent));
    this.localSHA1 = CryptoUtils.bytesToHex(toEncrypt.close());
    if (this.remoteSHA1 == null) {
      this.remoteSHA1 = localSHA1;
    }
    dirty = remoteSHA1 != localSHA1;
  }

  void doHash() {
    if (localSHA1correct) return;
    var toEncrypt = new SHA1();
    toEncrypt.add(encodeUtf8(_cachedContent));
    localSHA1 = CryptoUtils.bytesToHex(toEncrypt.close());
    localSHA1correct = true;
    if (localSHA1 == remoteSHA1) {
      if (dirty) events.fire(workspaceFileMarkClean, info);
      dirty = false;
    } else {
      if (!dirty) events.fire(workspaceFileMarkDirty, info);
      dirty = true;
    }
  }
}
class Workspace {
  final String workspaceID;
  IRepoInfo _info;
  Future<IRepoInfo> futureRepo;
  IRepoInfo get info => _info;
  static EventBus events;
  set info(IRepoInfo info) {
    _info = info;
    _updateLocalFiles(info.files);
  }

  Map<String, LocalFile> _cachedContent = {};
  Map<String, IFileInfo> _localUnsavedFiles = {};
  Map<String, Future<String>> cachedRequests = {};
  String _clientID;

  static Future<Workspace> createWorkspace(String workspaceName, String workspaceEMail, {String cloneFrom}) {
    assert(workspaceName != null);
    assert(workspaceEMail != null);
    Completer completer = new Completer();
    var dataUrl = getServerURL() + "workspace";
    var data = {
      'name': workspaceName,
      'eMail': workspaceEMail
    };
    if (cloneFrom != null) {
      data['cloneFrom'] = cloneFrom;
    }
    var encodedData = _encodeMap(data);
    var activity = new Activity("workspaceCreation", "Creating workspace");
    events.fire(activityStarted, activity);
    var httpRequest = new HttpRequest();
    httpRequest.open('POST', dataUrl);
    httpRequest.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    httpRequest.setRequestHeader('Accept', 'text/plain');
    httpRequest.onLoadEnd.listen((e) {
      events.fire(activityEnded, activity);
      var responseText = httpRequest.responseText;
      if (httpRequest.status != 201) {
        events.fire(workspaceError, httpRequest);
        completer.completeError(httpRequest);
      } else {
        var workspace = new Workspace(responseText.substring(responseText.lastIndexOf("/") + 1).trim());
        currentWorkspace = workspace;
        events.fire(workspaceCreated, null);
        completer.complete(workspace);
      }
    });
    httpRequest.send(encodedData);
    return completer.future;
  }

  static String getServerURL() {
    var server = getServer();
    var dataUrl = "$server/api/v0.1/";
    return dataUrl;
  }

  static getServer() {
    var server;
    if (isTesting()) {
      server = "localhost:"+window.location.port;
    } else {
      server = "api.pshdl.org";
    }
    return window.location.protocol+"//$server";
  }

  static bool isTesting() => window.location.host.contains("localhost") || window.location.host.contains("127.0.0.1") || window.location.protocol.startsWith("file");

  Future<IFileInfo> createFile(FileType type, String content, String fileName) {
    Completer complete = new Completer();
    var info = new FileInfo.empty();
    info.type = type;
    info.record = new FileRecord.empty()
        ..relPath = fileName
        ..lastModified = new DateTime.now().millisecondsSinceEpoch;
    info.record.hash = _checkSum(content);
    updateFileContent(info, content);
    saveFile(info).then((_) {
      events.fire(workspaceFileAdded, [info]);
      complete.complete(info);
    });
    return complete.future;
  }

  static String _checkSum(String content) {
    var toEncrypt = new SHA1();
    toEncrypt.add(encodeUtf8(content));
    return CryptoUtils.bytesToHex(toEncrypt.close());
  }

  Workspace(this.workspaceID) {
    currentWorkspace = this;
    futureRepo = loadFiles();
    futureRepo.then((repo) => events.fire(workspaceOpened, repo), onError: (err) => events.fire(workspaceError, err));
    attachToEventSource();
    new Timer.periodic(new Duration(seconds: 2), (_) {
      _cachedContent.values.forEach((LocalFile lf) {
        lf.doHash();
      });
    });
  }

  void attachToEventSource() {
    var httpRequest = new HttpRequest();
    var streamingBase = getServerURL() + "streaming/workspace/$workspaceID/";
    httpRequest.open('GET', streamingBase + "clientID");
    httpRequest.setRequestHeader('Content-type', 'text/plain');
    httpRequest.setRequestHeader('Accept', 'text/plain');
    httpRequest.onLoadEnd.listen((e) {
      var responseText = httpRequest.responseText;
      if (httpRequest.status != 200) {
        print("Failed to generate clientID for EventStream ${httpRequest.status}:" + responseText);
      } else {
        _clientID = responseText.trim();
        EventSource es = new EventSource("$streamingBase$_clientID/sse");
        es.onError.forEach((e) {
          print("An error occured with the event stream: $e");
        });
        es.onOpen.forEach((e) {
          postMessage(IMessage.CLIENT_CONNECTED, null, null);
        });
        es.onMessage.forEach((MessageEvent me) {
          var msg = new Message.fromJsonString(me.data);
          if (msg.subject != 'P:PING') print("SSE ${msg.subject}");
          if (msg.clientID != _clientID) events.fire(messageReceived, msg);
          switch (msg.subject) {
            case IMessage.WORK_ADDED:
              _filesUpdatedAdded(msg, workspaceFileAdded);
              break;
            case IMessage.WORK_UPDATED:
              _filesUpdatedAdded(msg, workspaceFileUpdated);
              break;
            case IMessage.WORK_DELETED:
              _removeFile(msg);
              break;
            case IMessage.WORK_CREATED_WORKSPACE:
              info = new RepoInfo.fromJson(msg.contents);
              events.fire(workspaceCreated, info);
              break;
          }
        });
      }
    });
    httpRequest.send();
  }

  void _removeFile(msg) {
    var fileInfo = new FileInfo.fromJson(msg.contents);
    var list = info.files.toList();
    var relPath = fileInfo.record.relPath;
    list.removeWhere((fi) => fi.record.relPath == relPath);
    //info.files is just a view not backing the original list
    info.files = list;
    var localFile = _cachedContent[relPath];
    if (localFile != null) {
      if (localFile.dirty) {
        _localUnsavedFiles[relPath] = localFile.info;
      } else {
        _cachedContent.remove(relPath);
      }
    }
    events.fire(workspaceFileDeleted, fileInfo);
  }

  void _filesUpdatedAdded(msg, eventType) {
    var files = msg.contents.map((map) => new FileInfo.fromJson(map)).toList();
    _updateLocalFiles(files);
    events.fire(eventType, files);
  }

  void _updateLocalFiles(Iterable<IFileInfo> files) {
    var names = files.map((fi) => fi.record.relPath).toSet();
    var repoFiles = info.files.toList();
    repoFiles.removeWhere((fi) => names.contains(fi.record.relPath));
    repoFiles.addAll(files);
    //info.files is just a view not backing the original list
    files.forEach((fi) {
      var localFile = _cachedContent[fi.record.relPath];
      if (localFile != null) {
        if (localFile.dirty) {
          localFile.info = fi;
          if (fi.record.hash != null) {
            localFile.remoteSHA1 = fi.record.hash;
            localFile.localSHA1correct = false;
          }
        } else {
          _cachedContent.remove(fi.record.relPath);
        }
      }
    });
    info.files = repoFiles;
  }

  Future<Null> postMessage(String subject, String type, dynamic content) {
    var message = new Message.empty()
        ..clientID = _clientID
        ..timeStamp = new DateTime.now().millisecondsSinceEpoch
        ..subject = subject;
    if (type != null) message.msgType = type;
    if (content != null) message.contents = content;
    var jsonString = message.toString();
    var httpRequest = new HttpRequest();
    var streamingBase = getServerURL() + "streaming/workspace/$workspaceID/$_clientID/";
    httpRequest.open('POST', streamingBase);
    httpRequest.setRequestHeader('Content-type', 'application/json');
    httpRequest.setRequestHeader('Accept', 'application/json');
    var completer = new Completer();
    httpRequest.onLoadEnd.listen((e) {
      var responseText = httpRequest.responseText;
      if (httpRequest.status != 204) {
        completer.completeError(httpRequest);
        print("Failed to post message: $message to EventStream ${httpRequest.status}:" + responseText);
      } else {
        completer.complete();
      }
    });
    httpRequest.send(jsonString);
    return completer.future;
  }

  Future<RepoInfo> loadFiles() {
    var dataUrl = getServerURL() + "workspace/$workspaceID";
    var httpRequest = new HttpRequest();
    httpRequest.open('GET', dataUrl);
    httpRequest.setRequestHeader('Accept', 'application/json');
    var completer = new Completer();
    var activity = new Activity("loadFiles", "Loading workspace information");
    events.fire(activityStarted, activity);
    httpRequest.onLoadEnd.listen((e) {
      events.fire(activityEnded, activity);
      if (httpRequest.status != 200) {
        events.fire(workspaceError, httpRequest);
        completer.completeError(httpRequest);
      } else {
        info = new RepoInfo.fromJsonString(httpRequest.responseText);
        completer.complete(info);
      }
    });
    httpRequest.send();
    return completer.future;
  }

  void updateFileContent(IFileInfo info, String content) {
    if (info != null) {
      var localFile = _cachedContent[info.record.relPath];
      if (localFile == null) {
        _cachedContent[info.record.relPath] = new LocalFile(info, content, events);
      } else {
        localFile.cachedContent = content;
      }
    }
  }

  bool isDirty(IFileInfo info) {
    var localFile = _cachedContent[info.record.relPath];
    if (localFile == null) return false;
    localFile.doHash();
    return localFile.dirty;
  }


  Future<List<dynamic>> saveAllFiles() {
    List<Future<Null>> futures = [];
    for (var fi in currentWorkspace.info.files) {
      futures.add(currentWorkspace.saveFile(fi));
    }
    return Future.wait(futures);
  }

  Future<Null> saveFile(IFileInfo info) {
    assert(info != null);
    var name = info.record.relPath;
    assert(name != null);
    var completer = new Completer();
    var localFile = _cachedContent[name];
    if (localFile == null) {
      print("Do not have a localfile for ${name}");
      completer.complete();
      return completer.future;
    }
    String content = localFile.cachedContent;
    var activity = new Activity("saveFile", "Saving files: ${name}");
    events.fire(activityStarted, activity);
    var dataUrl = getServerURL() + "workspace/$workspaceID";
    var params = _encodeMap({
      "file": content,
      "fileName": name
    });
    print("POST on $dataUrl");
    var httpRequest = new HttpRequest();
    httpRequest.open('POST', dataUrl);
    httpRequest.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    httpRequest.setRequestHeader('Accept', 'text/plain');
    httpRequest.onLoadEnd.listen((e) {
      events.fire(activityEnded, activity);
      if (httpRequest.status != 201) {
        events.fire(workspaceError, httpRequest);
        completer.completeError(httpRequest);
      } else {
        completer.complete();
      }
    });
    httpRequest.send(params);
    return completer.future;
  }

  Future<String> loadFile(FileInfo info, [bool forceLoad = false]) {
    assert(info != null);
    return loadFileByName(info.record.relPath, info, forceLoad);
  }

  Future<String> loadFileByName(String name, [FileInfo info = null, bool forceLoad = false]) {
    var localFile = _cachedContent[name];
    if (!forceLoad && localFile != null) {
      Completer completer = new Completer();
      completer.complete(localFile.cachedContent);
      return completer.future;
    }
    var future = cachedRequests[name];
    if (future != null) return future;
    Completer completer = new Completer();
    cachedRequests[name] = completer.future;
    var activity = new Activity("loadFile", "Loading file: ${name}");
    events.fire(activityStarted, activity);
    var dataUrl = getServerURL() + "workspace/$workspaceID/${name.replaceAll('/', ':')}";
    print("GET on $dataUrl");
    var httpRequest = new HttpRequest();
    httpRequest.open('GET', dataUrl);
    httpRequest.setRequestHeader('Accept', 'text/plain');
    httpRequest.onLoadEnd.listen((ProgressEvent e) {
      cachedRequests[name] = null;
      events.fire(activityEnded, activity);
      if (httpRequest.status != 200) {
        events.fire(workspaceError, httpRequest);
        completer.completeError(httpRequest);
      } else {
        String content = httpRequest.responseText;
        if (localFile == null) {
          if (info == null) {
            info = getFile(name);
            if (info == null) {
              completer.completeError(name);
              return;
            }
          }
          if (info != null) {
            localFile = new LocalFile(info, content, events);
            _cachedContent[name] = localFile;
          }
        }
        localFile.cachedContent = content;
        if (info != null) events.fire(workspaceFileUpdated, [info]);
        completer.complete(content);
      }
    });
    httpRequest.send();
    return completer.future;
  }

  Future<String> getConstraints(String settingsFile, String type) {
    assert(info != null);
    var completer = new Completer<String>();
    var activity = new Activity("constaint", "Retrieving constraints for $settingsFile");
    events.fire(activityStarted, activity);
    //boards/{wid}/constraints/{fileName}/{type}
    var dataUrl = getServerURL() + "boards/$workspaceID/constraints/$settingsFile/$type";
    print("POST on $dataUrl");
    var httpRequest = new HttpRequest();
    httpRequest.open('POST', dataUrl);
    httpRequest.setRequestHeader('Accept', 'text/plain');
    httpRequest.onLoadEnd.listen((e) {
      events.fire(activityEnded, activity);
      if (httpRequest.status != 200) {
        events.fire(workspaceError, httpRequest);
        completer.completeError(httpRequest);
      } else {
        completer.complete(httpRequest.responseText);
      }
    });
    httpRequest.send();
    return completer.future;
  }
  Future<Null> deleteFile(FileInfo info) {
    assert(info != null);
    var completer = new Completer<Null>();
    var relPath = info.record.relPath;
    if (_localUnsavedFiles.containsKey(relPath)) {
      _localUnsavedFiles.remove(relPath);
      completer.complete();
      return completer.future;
    }
    var activity = new Activity("deleteFile", "Deleting file:${relPath}");
    events.fire(activityStarted, activity);
    var dataUrl = getServerURL() + "workspace/$workspaceID/${relPath}";
    print("DELETE on $dataUrl");
    var httpRequest = new HttpRequest();
    httpRequest.open('DELETE', dataUrl);
    httpRequest.setRequestHeader('Accept', 'text/plain');
    httpRequest.onLoadEnd.listen((e) {
      events.fire(activityEnded, activity);
      if (httpRequest.status != 200) {
        events.fire(workspaceError, httpRequest);
        completer.completeError(httpRequest);
      } else {
        completer.complete();
      }
    });
    httpRequest.send();
    return completer.future;
  }

  void downloadFileRecord(IFileRecord info) {
    assert(info != null);

    IFrameElement frame = querySelector("#downloadFrame");
    if (info.fileURI.startsWith("http")) frame.src = info.fileURI; else frame.src = getServer() + info.fileURI + "?download=true&plain=true";
  }

  void downloadZip(bool generated) {
    var viewerFrame = querySelector("#downloadFrame") as IFrameElement;
    if (generated) viewerFrame.src = "${Workspace.getServerURL()}workspace/$workspaceID/srcGen.zip?download=true"; else viewerFrame.src = "${Workspace.getServerURL()}workspace/$workspaceID/workspace.zip?download=true";
  }

  void downloadFile(FileInfo info) {
    assert(info != null);
    downloadFileRecord(info.record);
  }

  Future<RepoInfo> validateWorkspace() {
    Completer completer = new Completer();
    var activity = new Activity("validateWorkspace", "Validating workspace");
    events.fire(activityStarted, activity);
    var dataUrl = getServerURL() + "compiler/$workspaceID/validate";
    var httpRequest = new HttpRequest();
    httpRequest.open('POST', dataUrl);
    print("POST on $dataUrl");
    httpRequest.setRequestHeader('Accept', 'application/json');
    httpRequest.onLoadEnd.listen((e) {
      events.fire(activityEnded, activity);
      if (httpRequest.status != 200) {
        events.fire(workspaceError, httpRequest);
        completer.completeError(httpRequest);
      } else {
        info = new RepoInfo.fromJsonString(httpRequest.responseText);
        events.fire(workspaceRepoUpdate, info);
        completer.complete(info);
      }
    });
    httpRequest.send();
    return completer.future;
  }

  Future<IModuleInformation> getModuleInformation(String module, Map context) {
    Completer completer = new Completer();
    var activity = new Activity("instanceInfo", "Requesting information about: $module");
    events.fire(activityStarted, activity);
    var dataUrl = getServerURL() + "compiler/$workspaceID/instanceInfo/$module?${_encodeMap(context)}";
    var httpRequest = new HttpRequest();
    httpRequest.open('POST', dataUrl);
    print("POST on $dataUrl");
    httpRequest.setRequestHeader('Accept', 'application/json');
    httpRequest.onLoadEnd.listen((e) {
      events.fire(activityEnded, activity);
      if (httpRequest.status != 200) {
        events.fire(workspaceError, httpRequest);
        completer.completeError(httpRequest);
      } else {
        var info = new ModuleInformation.fromJsonString(httpRequest.responseText);
        completer.complete(info);
      }
    });
    httpRequest.send();
    return completer.future;
  }

  Future<String> compileToDart(FileInfo file, [Map<String, String> arguments]) {
    var dataUrl = getServerURL() + "compiler/$workspaceID/psex/";
    Completer completer = new Completer();
    var httpRequest = new HttpRequest();
    String target = "JavaScript";
    var doDart = isDart();
    String type;
    if (doDart) {
      target = "Dart";
      type="dart";
    } else {
      type="javascript";
    }
    var activity = new Activity("dartcomp", "Compiling ${file.record.relPath} to $target");
    events.fire(activityStarted, activity);
    httpRequest.open('POST', dataUrl+type);
    httpRequest.setRequestHeader('Accept', 'application/'+type);
    httpRequest.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    httpRequest.onLoadEnd.listen((e) {
      events.fire(activityEnded, activity);
      if (httpRequest.status != 201) {
        events.fire(workspaceError, httpRequest);
        completer.completeError(httpRequest);
      } else {
        var resUrl = httpRequest.responseText;
        if (doDart) completer.complete(resUrl); else {
          completer.complete(resUrl.trim());
        }
      }
    });
    var data = {
      "asURI": "true",
      "file": file.record.relPath
    };
    if (arguments != null) data.addAll(arguments);
    httpRequest.send(_encodeMap(data));
    return completer.future;
  }


  Future<List<String>> compileWorkspace(CodeTarget target, [Map<String, String> arguments]) {
    String typeURI = "";
    switch (target) {
      case CodeTarget.C:
        typeURI = "psex/c";
        break;
      case CodeTarget.JAVA:
        typeURI = "psex/java";
        break;
      case CodeTarget.DART:
        typeURI = "psex/dart";
        break;
      case CodeTarget.JAVA_SCRIPT:
        typeURI = "psex/javascript";
        break;
      case CodeTarget.VHDL:
        typeURI = "vhdl";
        break;
    }
    var dataUrl = getServerURL() + "compiler/$workspaceID/$typeURI";
    var httpRequest = new HttpRequest();
    httpRequest.open('POST', dataUrl);
    Completer completer = new Completer();
    var activity = new Activity("workspaceComp", "Compiling workspace");
    events.fire(activityStarted, activity);
    print("POSTing to $dataUrl");
    httpRequest.setRequestHeader('Accept', 'text/plain');
    httpRequest.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    httpRequest.onLoadEnd.listen((e) {
      events.fire(activityEnded, activity);
      if (httpRequest.status != 200) {
        events.fire(workspaceError, httpRequest);
        completer.completeError(httpRequest);
      } else {
        completer.complete(httpRequest.responseText.split("\n").where((String s) => s.trim().isNotEmpty).toList());
      }
    });
    httpRequest.send(_encodeMap(arguments));
    return completer.future;
  }


  String getFileURL(String file) {
    return getServerURL() + "workspace/$workspaceID/$file";
  }

  Future<IFileInfo> getFileAsync(String newValue) {
    Completer completer=new Completer();
    if (info!=null){
      completer.complete(getFile(newValue));
    } else {
      futureRepo.then((repo){
        completer.complete(repo.files.firstWhere((fi) => fi.record.relPath == newValue, orElse: () => null));
      });
    }
    return completer.future;
  }
  
  IFileInfo getFile(String newValue) {
    return info.files.firstWhere((fi) => fi.record.relPath == newValue, orElse: () => null);
  }

  static FileType guessFileType(String file) {
    var lastIndexOf = file.lastIndexOf('\.');
    var extension = "";
    if (lastIndexOf > 0) {
      extension = file.substring(lastIndexOf);
    }
    switch (extension) {
      case "pshdl":
        return FileType.pshdl;
      case "vhdl":
        return FileType.vhdl;
      case "vhd":
        return FileType.vhdl;
      case "v":
        return FileType.verilog;
      case "sv":
        return FileType.verilog;
      case "md":
        return FileType.markdown;
      case "markdown":
        return FileType.markdown;
      case "c":
        return FileType.cpp;
      case "cpp":
        return FileType.cpp;
      case "h":
        return FileType.cHeader;
      case "htm":
        return FileType.html;
      case "html":
        return FileType.html;
      case "dart":
        return FileType.dart;
      case "json":
        return FileType.json;
      case "js":
        return FileType.javascript;
      default:
        return FileType.unknown;
    }
  }

  Future<Null> renameFile(IFileInfo fi, String newFileName) {
    assert(fi != null);
    var completer = new Completer<Null>();
    var relPath = fi.record.relPath;
    var activity = new Activity("renameFile", "Renaming file:${relPath} to $newFileName");
    events.fire(activityStarted, activity);
    var dataUrl = getServerURL() + "workspace/$workspaceID/${relPath}/rename";
    print("POST on $dataUrl");
    var httpRequest = new HttpRequest();
    httpRequest.open('POST', dataUrl);
    httpRequest.setRequestHeader('Accept', 'text/plain');
    httpRequest.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    httpRequest.onLoadEnd.listen((e) {
      events.fire(activityEnded, activity);
      if (httpRequest.status != 200) {
        events.fire(workspaceError, httpRequest);
        completer.completeError(httpRequest);
      } else {
        completer.complete();
      }
    });
    httpRequest.send(_encodeMap({
      'newFileName': newFileName
    }));
    return completer.future;
  }

  static String _encodeMap(Map data) {
    if (data == null) return "";
    return data.keys.map((k) {
      assert(k != null);
      if (data[k] == null) print("Null value for key:$k in map $data");
      assert(data[k] != null);
      return '${Uri.encodeComponent(k)}=${Uri.encodeComponent(data[k])}';
    }).join('&');
  }
}
