library simm_comm_js;
import '../simulation_comm.dart';
import 'dart:isolate';
import 'dart:js' as js;
import 'dart:convert';

var _mod = js.context['Module'];

class DartInterpreterJS extends DartInterpreter {
  
  js.JsFunction _simRun;
  js.JsFunction _simInitConstants;
  js.JsFunction _getOutput;
  js.JsFunction _getOutputArr;
  js.JsFunction _setInput;
  js.JsFunction _setInputArr;
  js.JsFunction _getDeltaCycle;
  js.JsFunction _getVarNum;
  js.JsFunction _getDescription;
  js.JsFunction _getName;
  Description _desc;
  DartInterpreterJS(bool edges, bool outputLogic){
    js.JsFunction wrap=_mod.callMethod('cwrap',['pshdl_sim_setDisableEdges', 'void', ['number']]);
    wrap.apply([edges]);
    wrap=_mod.callMethod('cwrap',['pshdl_sim_setDisableRegOutputlogic', 'void', ['number']]);
    wrap.apply([outputLogic]);
    _simRun=_mod.callMethod('cwrap',['pshdl_sim_run', 'void', []]);
    _simInitConstants=_mod.callMethod('cwrap',['pshdl_sim_initConstants', 'void', []]);
    _getDeltaCycle=_mod.callMethod('cwrap',['pshdl_sim_getDeltaCycle', 'number', []]);
    _getVarNum=_mod.callMethod('cwrap',['pshdl_sim_getVarCount', 'number', []]);
    _getDescription=_mod.callMethod('cwrap',['pshdl_sim_getJsonDesc', 'string', []]);
    _getName=_mod.callMethod('cwrap',['pshdl_sim_getName', 'string', ['number']]);
    _getOutput=_mod.callMethod('cwrap',['pshdl_sim_getOutput', 'number', ['number']]);
    _getOutputArr=_mod.callMethod('cwrap',['pshdl_sim_getOutputArray', 'number', ['number','number']]);
    _setInput=_mod.callMethod('cwrap',['pshdl_sim_setInput', 'void', ['number','number']]);
    _setInputArr=_mod.callMethod('cwrap',['pshdl_sim_setInputArray', 'void', ['number','number','number']]);
    _desc=_description();
  }
  
  @override
  void run(){
    _simRun.apply([]);
  }

  @override
  int getVar(int idx, {int offset:-1}){
    if (offset!=-1)
      return _getOutputArr.apply([idx, offset]);
    return _getOutput.apply([idx]);
  }
  
  @override
  void setVar(int idx, dynamic value, {int offset:-1}){
    if (offset!=-1)
      _setInputArr.apply([idx, offset, value]);
    _setInput.apply([idx, value]);
  }
  
  @override
  int get deltaCycle{
    return _getDeltaCycle.apply([]);
  }
  
  @override
  int get varNum{
    return _getVarNum.apply([]);
  }
  
  @override
  Description get description{
    return _desc;
  }

  Description _description() {
    var str=_getDescription.apply([]);
    var map=JSON.decode(str);
    return new Description.fromMap(map);
  }

  @override
  int getIndex(String name) {
    return _desc.nameIdx[name];
  }

  @override
  String getName(int idx) {
    return _getDescription.apply([idx]);
  }
}


void main(List<String> args, SendPort replyTo){
  handleReceive((e,l) => new DartInterpreterJS(e,l), replyTo);
}